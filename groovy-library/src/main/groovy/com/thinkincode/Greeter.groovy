package com.thinkincode

import groovy.transform.CompileStatic

@CompileStatic
class Greeter {
    static String greet(String name) {
        "Hello, ${name.capitalize()}"
    }
}