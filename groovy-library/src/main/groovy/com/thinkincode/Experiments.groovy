package com.thinkincode

println "===== String experiments ====="

firstName = "Homer"
fullName = "${firstName} Simpson"

println "firstName.getClass() = ${firstName.getClass()}"
println "fullName.getClass() = ${fullName.getClass()}"

multilineString = '''\
  line one
  line two
  line three'''

println "multilineString = $multilineString"
println "multilineString.stripIndent() = ${multilineString.stripIndent()}"

println "===== Class experiments ===="

println "Current class = ${getClass()}"

class SomeStringProvider {
  String provide() {
      def x = "This shouldn't be returned!"
      def y = "This should be returned!" //no need for semicolon and no need for return statement
  }
}

println new SomeStringProvider().provide()

println "===== Dynamic variable experiments ===="

x = new Date()
println "x = $x"
x = 3.0 + 0.2
println "x = $x"

println "===== Variable binding experiments ===="

x = "Hello!"
void printX() {
  println "x = $x"
}
printX()

println "===== List experiments ===="

jvmLanguages = ["Java", "Groovy"]
jvmLanguages.add("Scala")
jvmLanguages[3] = "Clojure"
jvmLanguages[5] = "A sixth language!"
jvmLanguages << "A seventh language!"
println "jvmLanguages = $jvmLanguages"
println "jvmLanguages[0,1,2] = ${jvmLanguages[0,1,2]}"
println "jvmLanguages[0..2] = ${jvmLanguages[0..2]}"
println "jvmLanguages[2..0] = ${jvmLanguages[2..0]}"
println "jvmLanguages[3] = ${jvmLanguages[3]}"
println "jvmLanguages[4] = ${jvmLanguages[4]}"
println "jvmLanguages[-2..-1] = ${jvmLanguages[-2..-1]}"
println "jvmLanguages[-1..-2] = ${jvmLanguages[-1..-2]}"
println "jvmLanguages[-3] = ${jvmLanguages[-3]}"
println "jvmLanguages[-4] = ${jvmLanguages[-4]}"

mixedTypes = ["Java", 2, new Date()]
println "mixedTypes = $mixedTypes"

println "===== Map experiments ===="

languageRatings = [Java:100, Clojure:"N/A"]
println "languageRatings = $languageRatings"

languageName = "Scala"
languageRatings[(languageName)] = 55
println "languageRatings = $languageRatings"

languageRatings["Java"] = 75
println "Java rating = " + languageRatings["Java"]

languageRatings.Java = 65
println "Java rating = $languageRatings.Java"

println "Java language rating == 65? " + (languageRatings["Java"] == 65)
println languageRatings["Java"].getClass()

println "Clojure language rating == \"N/A\"? " + (languageRatings["Clojure"] == "N/A")
println languageRatings["Clojure"].getClass()

languageRatings = [:]
println languageRatings
println languageRatings.keySet().size()
println languageRatings.values().size()

println "===== Equality experiments ===="

x = new Integer(2)
y = new Integer(2)
z = null

if (x == y) { // implicit equals() called
    println "x == y"
}
if (!x.is(y)) { // object identity check
    println "x is not y"
}
if (z.is(null)) { // check for null with is()
    println "z is null"
}
if (z == null) { // check for null
    println "z == null"
}

println "===== Closure experiments ====="

number = 1
eagerGString = "value == ${number}"
lazyGString = "value == ${ -> number }"

println eagerGString
println lazyGString

number = 2

println eagerGString
println lazyGString

println "===== Number experiments ====="

def a = 1
println "a.getClass() == ${a.getClass()}"
assert a instanceof Integer

def b = 2147483647 // Integer.MAX_VALUE
println "b.getClass() == ${b.getClass()}"
assert b instanceof Integer

def c = 2147483648 // Integer.MAX_VALUE + 1
println "c.getClass() == ${c.getClass()}"
assert c instanceof Long

def d = 9223372036854775807 // Long.MAX_VALUE
println "d.getClass() == ${d.getClass()}"
assert d instanceof Long

def e = 9223372036854775808 // Long.MAX_VALUE + 1
println "e.getClass() == ${e.getClass()}"
assert e instanceof BigInteger

int xInt = 0b11
println "0b11 as int == $xInt"
assert xInt == 3 as int

short xShort = 0b11
println "0b11 as sho == $xShort"
assert xShort == 3 as short

byte xByte = 0b11
println "0b11 as byte == $xByte"
assert xByte == 3 as byte

println "1e3 == ${1e3}"
assert 1e3  ==  1_000.0

println "===== GroovyBean experiments ====="

class Character {
    private int strength
    private int wisdom
}

def character = new Character(strength: 10, wisdom: 15)
character.strength = 18
println "STR [" + character.strength + "] WIS [" + character.wisdom + "]"

class Person {
    private String name
    
    public String getName() {
        "name = $name"
    }
}

person = new Person(name: "Adil")
println person.@name
println person.name

println "===== Safe-deference experiments ====="

people = [null, new Person(name:"Gweneth")]
for (Person person: people) {
  println person?.name
}

println "===== Function literal experiments ====="

def sayHello = { name -> name == "Martijn" || name == "Ben" ? "Hello author " + name + "!" : "Hello reader " + name + "!" }
println sayHello("Ben")
println sayHello("Tim")

movieTitles = ["Seven", "SnowWhite", "Die Hard"]
movieTitles.each({x -> println x})
movieTitles.each({println it})
