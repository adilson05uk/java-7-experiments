package com.thinkincode.java.concurrency;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static com.thinkincode.java.utils.ThreadUtils.getCurrentThreadName;

@Ignore
@RunWith(JUnit4.class)
public class DeadlockExperiment3Test {

    @Test(timeout = 1_000)
    public void test_deadlock() throws Exception {
        DeadlockExperiment3 deadlockExperimentA = new DeadlockExperiment3();
        DeadlockExperiment3 deadlockExperimentB = new DeadlockExperiment3();

        Thread threadA = new Thread(() -> {
            System.out.println("Entering Thread '" + getCurrentThreadName() + "'");
            deadlockExperimentA.methodA(deadlockExperimentB);
            System.out.println("Exiting Thread '" + getCurrentThreadName() + "'");
        });
        Thread threadB = new Thread(() -> {
            System.out.println("Entering Thread '" + getCurrentThreadName() + "'");
            deadlockExperimentB.methodA(deadlockExperimentA);
            System.out.println("Exiting Thread '" + getCurrentThreadName() + "'");
        });

        threadA.start();
        threadB.start();

        threadA.join();
        threadB.join();
    }
}
