package com.thinkincode.java.concurrency;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static com.thinkincode.java.utils.ThreadUtils.getCurrentThreadName;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class IncrementAtomicIntegerThreadTest {

    private final static int NUMBER_OF_THREADS = 1_000;

    private AtomicInteger someNumber;

    @Before
    public void setUp() {
        someNumber = new AtomicInteger(0);
    }

    @Test(timeout = 1_000)
    public void test_increment() throws Exception {
        // Given.
        List<Thread> threads = new ArrayList<>();

        IntStream.range(0, NUMBER_OF_THREADS)
                .forEach(i -> threads.add(createThread()));

        // When.
        for (Thread thread : threads) {
            thread.start();
        }

        for (Thread thread : threads) {
            thread.join();
        }

        // Then.
        assertEquals(NUMBER_OF_THREADS, someNumber.get());
    }

    private Thread createThread() {
        return new Thread(() -> {
            try {
                Thread.sleep(1);
                System.out.println("Thread '" + getCurrentThreadName() + "': Number after increment = " + someNumber.incrementAndGet());
            } catch (InterruptedException e) {
                System.out.println("Thread '" + getCurrentThreadName() + "': Caught InterruptedException");
            }
        });
    }
}
