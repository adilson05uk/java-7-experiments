package com.thinkincode.java.concurrency;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;

@RunWith(JUnit4.class)
public class CopyOnWriteArrayListTest {

    @Test
    public void test_iterator() {
        // Given.
        List<String> inputList = new CopyOnWriteArrayList<>();

        Iterator<String> iterator1;
        Iterator<String> iterator2;

        inputList.add("A");

        // When.
        iterator1 = inputList.iterator();

        inputList.add("B");

        // Then.
        List<String> outputList1 = new ArrayList<>();
        List<String> outputList2 = new ArrayList<>();

        while (iterator1.hasNext()) {
            outputList1.add(iterator1.next());
        }

        iterator2 = inputList.iterator();

        while (iterator2.hasNext()) {
            outputList2.add(iterator2.next());
        }

        assertThat(outputList1, contains("A"));
        assertThat(outputList2, contains("A", "B"));
    }
}
