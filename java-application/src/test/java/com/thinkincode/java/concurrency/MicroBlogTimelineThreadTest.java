package com.thinkincode.java.concurrency;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@RunWith(JUnit4.class)
public class MicroBlogTimelineThreadTest {

    @Test
    public void test_printTimeline() throws Exception {
        CountDownLatch firstLatch = new CountDownLatch(1);
        CountDownLatch secondLatch = new CountDownLatch(1);

        Lock lock = new ReentrantLock();

        List<String> updates = new CopyOnWriteArrayList<>();
        updates.add("A");
        updates.add("B");

        MicroBlogTimeline tl1 = new MicroBlogTimeline("TL1", lock, updates);
        MicroBlogTimeline tl2 = new MicroBlogTimeline("TL2", lock, updates);

        Thread t1 = new Thread(() -> {
            try {
                tl1.addUpdate("C");
                tl1.prepareIterator();

                firstLatch.countDown();
                secondLatch.await();
            } catch (InterruptedException e) {
                // nothing to do
            }

            tl1.printTimeline();
        });

        Thread t2 = new Thread(() -> {
            try {
                firstLatch.await();

                tl2.addUpdate("D");
                tl2.prepareIterator();

                secondLatch.countDown();
            } catch (InterruptedException e) {
                // nothing to do
            }

            tl2.printTimeline();
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();
    }
}