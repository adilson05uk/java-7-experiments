package com.thinkincode.java.concurrency;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static com.thinkincode.java.utils.ThreadUtils.getCurrentThreadName;

@Ignore
@RunWith(JUnit4.class)
public class DeadlockExperiment1Test {

    @Test(timeout = 1_000)
    public void test_deadlock() throws Exception {
        DeadlockExperiment1 deadlockExperimentA = new DeadlockExperiment1();
        DeadlockExperiment1 deadlockExperimentB = new DeadlockExperiment1();

        Thread threadA = new Thread(() -> {
            System.out.println("Entering Thread '" + getCurrentThreadName() + "'");
            deadlockExperimentA.methodA(deadlockExperimentB);
            System.out.println("Exiting Thread '" + getCurrentThreadName() + "'");
        });
        Thread threadB = new Thread(() -> {
            System.out.println("Entering Thread '" + getCurrentThreadName() + "'");
            deadlockExperimentB.methodA(deadlockExperimentA);
            System.out.println("Exiting Thread '" + getCurrentThreadName() + "'");
        });

        threadA.start();
        threadB.start();

        threadA.join();
        threadB.join();
    }
}
