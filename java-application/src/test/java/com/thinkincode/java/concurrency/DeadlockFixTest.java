package com.thinkincode.java.concurrency;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static com.thinkincode.java.utils.ThreadUtils.getCurrentThreadName;

@RunWith(JUnit4.class)
public class DeadlockFixTest {

    @Test(timeout = 1_000)
    public void test_deadlock() throws Exception {
        DeadlockFix deadlockFixA = new DeadlockFix();
        DeadlockFix deadlockFixB = new DeadlockFix();

        Thread threadA = new Thread(() -> {
            System.out.println("Entering Thread '" + getCurrentThreadName() + "'");
            deadlockFixA.methodA(deadlockFixB);
            System.out.println("Exiting Thread '" + getCurrentThreadName() + "'");
        });
        Thread threadB = new Thread(() -> {
            System.out.println("Entering Thread '" + getCurrentThreadName() + "'");
            deadlockFixB.methodA(deadlockFixA);
            System.out.println("Exiting Thread '" + getCurrentThreadName() + "'");
        });

        threadA.start();
        threadB.start();

        threadA.join();
        threadB.join();
    }
}
