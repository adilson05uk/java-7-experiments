package com.thinkincode.java;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

@RunWith(JUnit4.class)
public class ArrayTest {

    @Test
    public void test_converting_object_array_to_string_array_with_arrayCopy_method() {
        // Given.
        Object[] objectArray = { "a", "b", "c" };
        String[] stringArray = new String[objectArray.length];

        // When.
        System.arraycopy(objectArray, 0, stringArray, 0, objectArray.length);

        // Then.
        assertArrayEquals(stringArray, objectArray);
    }

    @Test
    public void test_converting_object_array_to_string_array_with_copyOf_method() {
        // Given.
        Object[] objectArray = { "a", "b", "c" };

        // When.
        String[] stringArray = Arrays.copyOf(objectArray, objectArray.length, String[].class);

        // Then.
        assertArrayEquals(stringArray, objectArray);
    }

    @Test(expected = ClassCastException.class)
    public void test_casting_object_array_to_string_array() {
        // Given.
        Object[] objectArray = { "a", "b", "c" };

        // When.
        String[] stringArray = (String[]) objectArray;
    }
}
