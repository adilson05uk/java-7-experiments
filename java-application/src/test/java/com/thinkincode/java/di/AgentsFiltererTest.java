package com.thinkincode.java.di;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class AgentsFiltererTest {

    @Test
    public void test_getFilteredAgents() {
        // Given.
        AgentFinder mockAgentFinder = mock(AgentFinder.class);

        Agent mockAgent1 = mock(Agent.class);
        Agent mockAgent2 = mock(Agent.class);
        Agent mockAgent3 = mock(Agent.class);

        List<Agent> agents = Arrays.asList(mockAgent1, mockAgent2, mockAgent3);

        when(mockAgentFinder.findAllAgents()).thenReturn(agents);
        when(mockAgent1.getType()).thenReturn("Type1");
        when(mockAgent2.getType()).thenReturn("Type2");
        when(mockAgent3.getType()).thenReturn("Type3");

        AgentsFilterer agentsFilterer = new AgentsFilterer(mockAgentFinder);

        // When.
        List<Agent> result = agentsFilterer.getFilteredAgents("Type2");

        // Then.
        assertEquals(Collections.singletonList(mockAgent2), result);
    }
}