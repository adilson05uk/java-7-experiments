package com.thinkincode.java.io;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

@RunWith(JUnit4.class)
public class FileVisitorExperimentTest {

    private PathExperiment pathExperiment;
    private FileVisitorExperiment fileVisitorExperiment;

    @Before
    public void setUp() {
        pathExperiment = new PathExperiment();
        fileVisitorExperiment = new FileVisitorExperiment();
    }

    @Test
    public void test_getNamesOfFilesMatchingGlob() throws Exception {
        // Given.
        Path parentPath = pathExperiment.getCurrentWorkingDirectoryPath();
        Path childPath = Paths.get("src","test", "resources", "IO");
        Path directoryPath = parentPath.resolve(childPath);

        // When.
        List<String> filenames = fileVisitorExperiment.getNamesOfFilesMatchingGlob(directoryPath, "*.properties");

        // Then.
        assertThat(filenames, containsInAnyOrder("File1.properties", "File2.properties", "File3.properties"));
    }
}