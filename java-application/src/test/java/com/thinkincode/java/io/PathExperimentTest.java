package com.thinkincode.java.io;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class PathExperimentTest {

    private PathExperiment pathExperiment;

    @Before
    public void setUp() {
        pathExperiment = new PathExperiment();
    }

    @Test
    public void test_getZipUtilityPath() {
        // When.
        Path path = pathExperiment.getZipUtilityPath();

        // Then.
        assertEquals("/usr/bin/zip", path.toString());
    }

    @Test
    public void test_getCurrentWorkingDirectoryPath() {
        // When.
        Path originalPath = pathExperiment.getCurrentWorkingDirectoryPath();

        // Then.
        assertEquals("java-application", originalPath.getFileName().toString());
    }

    @Test
    public void test_getNamesOfFilesMatchingGlob() throws IOException {
        // Given.
        Path parentPath = pathExperiment.getCurrentWorkingDirectoryPath();
        Path childPath = Paths.get("src","test", "resources", "IO");
        Path path = parentPath.resolve(childPath);

        // When.
        List<String> filenames = pathExperiment.getNamesOfFilesMatchingGlob(path, "*.properties");

        // Then.
        assertThat(filenames, containsInAnyOrder("File1.properties", "File2.properties"));
    }
}