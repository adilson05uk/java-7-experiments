package com.thinkincode.java.io;

import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FilesExperimentTest {

    private PathExperiment pathExperiment;
    private FilesExperiment filesExperiment;

    @Before
    public void setUp() throws Exception {
        pathExperiment = new PathExperiment();
        filesExperiment = new FilesExperiment();

        Path testOutputDirectoryPath = getTestOutputDirectoryPath();
        filesExperiment.recursivelyDeleteDirectory(testOutputDirectoryPath);
    }

    @Test
    public void test_createFileWithReadWritePermissions() throws Exception {
        // Given.
        Path testOutputDirectoryPath = getTestOutputDirectoryPath();

        // When.
        filesExperiment.createFileWithReadWritePermissions(testOutputDirectoryPath.resolve("Directory1"), "File1.txt");

        // Then.
        assertTrue(Files.exists(testOutputDirectoryPath.resolve("Directory1").resolve("File1.txt")));
    }

    @Test
    public void test_recursivelyDeleteDirectory() throws Exception {
        // Given.
        Path testOutputDirectoryPath = getTestOutputDirectoryPath();

        filesExperiment.createFileWithReadWritePermissions(testOutputDirectoryPath.resolve("Directory1"), "File1.txt");
        filesExperiment.createFileWithReadWritePermissions(testOutputDirectoryPath.resolve("Directory2"), "File2.txt");
        filesExperiment.createFileWithReadWritePermissions(testOutputDirectoryPath, "File3.txt");

        // When.
        filesExperiment.recursivelyDeleteDirectory(testOutputDirectoryPath);

        // Then.
        assertFalse(Files.exists(testOutputDirectoryPath));
    }

    private Path getTestOutputDirectoryPath() {
        Path parentPath = pathExperiment.getCurrentWorkingDirectoryPath();
        Path childPath = Paths.get("build", "test-output");
        return parentPath.resolve(childPath);
    }
}