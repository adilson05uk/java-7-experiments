package com.thinkincode.java.reflection;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

public class MethodHandleExperiment {

    public MethodHandle getMethodHandle(Object object, String methodName) throws NoSuchMethodException, IllegalAccessException {
        MethodType methodType = MethodType.methodType(String.class);
        MethodHandles.Lookup lookup = MethodHandles.lookup();

        return lookup.findVirtual(object.getClass(), methodName, methodType);
    }
}
