package com.thinkincode.java.di;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import java.util.List;
import java.util.stream.Collectors;

public class AgentsFilterer {

    private final AgentFinder finder;

    @Inject
    public AgentsFilterer(@Named("WebServiceAgentFinder") AgentFinder finder) {
        this.finder = finder;
    }

    public List<Agent> getFilteredAgents(String agentType) {
        List<Agent> agents = finder.findAllAgents();
        return filterAgents(agents, agentType);
    }

    private List<Agent> filterAgents(List<Agent> agents, String agentType) {
        return agents.stream()
                .filter(agent -> agent.getType().equals(agentType))
                .collect(Collectors.toList());
    }
}
