package com.thinkincode.java.di;

import com.google.inject.Singleton;

import java.util.Arrays;
import java.util.List;

@Singleton
public class SpreadsheetAgentFinder implements AgentFinder {

    @Override
    public List<Agent> findAllAgents() {
        return Arrays.asList(
                new Agent("Developer"),
                new Agent("Designer"),
                new Agent("Salesperson")
        );
    }
}