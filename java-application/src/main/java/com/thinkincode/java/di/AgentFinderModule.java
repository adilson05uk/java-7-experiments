package com.thinkincode.java.di;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class AgentFinderModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(AgentFinder.class)
                .annotatedWith(Names.named("SpreadsheetAgentFinder"))
                .to(SpreadsheetAgentFinder.class);

        bind(AgentFinder.class)
                .annotatedWith(Names.named("WebServiceAgentFinder"))
                .to(WebServiceAgentFinder.class);
    }
}
