package com.thinkincode.java.di;

import java.util.List;

public interface AgentFinder {

    List<Agent> findAllAgents();
}
