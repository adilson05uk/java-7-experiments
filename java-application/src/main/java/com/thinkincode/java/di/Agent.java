package com.thinkincode.java.di;

public class Agent {

    private final String type;

    public Agent(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Agent agent = (Agent) o;

        return type != null ? type.equals(agent.type) : agent.type == null;
    }

    @Override
    public int hashCode() {
        return type != null ? type.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Agent{" +
                "type='" + type + '\'' +
                '}';
    }
}
