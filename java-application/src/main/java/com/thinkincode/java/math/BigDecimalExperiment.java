package com.thinkincode.java.math;

import java.math.BigDecimal;

public class BigDecimalExperiment {

    public BigDecimal add(double val1, double val2) {
        BigDecimal x = new BigDecimal(val1);
        BigDecimal y = new BigDecimal(val2);

        return x.add(y);
    }

    public BigDecimal add(String val1, String val2) {
        BigDecimal x = new BigDecimal(val1);
        BigDecimal y = new BigDecimal(val2);

        return x.add(y);
    }
}
