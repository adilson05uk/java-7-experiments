package com.thinkincode.java;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.thinkincode.java.di.Agent;
import com.thinkincode.java.di.AgentFinderModule;
import com.thinkincode.java.di.AgentsFilterer;
import com.thinkincode.java.groovy.GroovyExperiment;
import com.thinkincode.java.io.PathExperiment;
import com.thinkincode.java.math.BigDecimalExperiment;
import com.thinkincode.java.performance.CacheExperiment;
import com.thinkincode.java.performance.NanoTimeExperiment;
import com.thinkincode.java.reflection.MethodHandleExperiment;
import com.thinkincode.java.reflection.Person;

import java.lang.invoke.MethodHandle;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        try {
            Main main = new Main();

//            main.runIOExperiments();
//            main.runDIExperiments();
//            main.runReflectionExperiments();
//            main.runPerformanceExperiments();
//            main.runMathExperiments();
            main.runGroovyExperiments(args);
        } catch (Throwable e) {
            System.err.print(e.toString());
        }
    }

    private void runIOExperiments() {
        PathExperiment pathExperiment = new PathExperiment();

        Path parentPath = pathExperiment.getCurrentWorkingDirectoryPath();
        Path childPath = Paths.get("src", "test", "resources", "IO");
        Path path = parentPath.resolve(childPath);

        pathExperiment.printInformationAboutPath(path);

//        FilesExperiment filesExperiment = new FilesExperiment();
//        filesExperiment.printInformationAboutFile(path);

//        WatchServiceExperiment watchServiceExperiment = new WatchServiceExperiment();
//        watchServiceExperiment.watchFile(path);

//        FutureExperiment futureExperiment = new FutureExperiment();
//        futureExperiment.readFileWithFuture(path.resolve("File1.txt"));

//        CallbackExperiment callbackExperiment = new CallbackExperiment();
//        callbackExperiment.readFileWithCallback(path.resolve("File1.txt"));
    }

    private void runDIExperiments() {
        Injector injector = Guice.createInjector(new AgentFinderModule());
        AgentsFilterer agentsFilterer = injector.getInstance(AgentsFilterer.class);
        List<Agent> agents = agentsFilterer.getFilteredAgents("Developer");

        System.out.println(agents);
    }

    private void runReflectionExperiments() throws Throwable {
        Person person = new Person(25, "Jim");

        MethodHandleExperiment methodHandleExperiment = new MethodHandleExperiment();

        MethodHandle getNameMethodHandle = methodHandleExperiment.getMethodHandle(person, "getName");
        MethodHandle toStringMethodHandle = methodHandleExperiment.getMethodHandle(person, "toString");

        Object getNameResult = getNameMethodHandle.invoke(person);
        Object toStringResult = toStringMethodHandle.invoke(person);

        System.out.println("Person.getName() = " + getNameResult);
        System.out.println("Person.toString() = " + toStringResult);
    }

    private void runPerformanceExperiments() {
        NanoTimeExperiment nanoTimeExperiment = new NanoTimeExperiment();
        CacheExperiment cacheExperiment = new CacheExperiment();

        nanoTimeExperiment.printNanos();
        cacheExperiment.run();
    }

    private void runMathExperiments() {
        BigDecimalExperiment bigDecimalExperiment = new BigDecimalExperiment();

        System.out.println(bigDecimalExperiment.add(3.0, 0.2));
        System.out.println(bigDecimalExperiment.add("3.0", "0.2"));
    }

    private void runGroovyExperiments(String[] args) {
        new GroovyExperiment().executeGroovyClass(args);
    }
}
