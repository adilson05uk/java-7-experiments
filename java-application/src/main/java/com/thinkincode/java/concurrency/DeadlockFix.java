package com.thinkincode.java.concurrency;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.thinkincode.java.utils.ThreadUtils.getCurrentThreadName;

public class DeadlockFix {

    private final static long MAX_WAIT_MILLIS = 10L;

    private final Lock lock = new ReentrantLock();

    public void methodA(DeadlockFix deadlockFix) {
        boolean done = false;

        while (!done) {
            long waitMillis = getRandomWaitMillis();

            boolean acquired = false;

            try {
                acquired = lock.tryLock(waitMillis, TimeUnit.MILLISECONDS);

                if (acquired) {
                    System.out.println("methodA(...) in Thread '" + getCurrentThreadName() + "' about to call tryMethodB(...)");
                    done = deadlockFix.tryMethodB();
                    System.out.println("methodA(...) in Thread '" + getCurrentThreadName() + "' got result " + done + " from tryMethodB(...)");
                }
            } catch (InterruptedException e) {
                // nothing to do
            } finally {
                if (acquired) {
                    lock.unlock();
                }
            }

            if (!done) {
                try {
                    Thread.sleep(waitMillis);
                } catch (InterruptedException e) {
                    // nothing to do
                }
            }
        }
    }

    public boolean tryMethodB() {
        long waitMillis = getRandomWaitMillis();

        boolean acquired = false;

        try {
            acquired = lock.tryLock(waitMillis, TimeUnit.MILLISECONDS);

            if (acquired) {
                System.out.println("tryMethodB(...) in Thread '" + getCurrentThreadName() + "' succeeded in acquiring the lock.");
                methodB();
                return true;
            } else {
                System.out.println("tryMethodB(...) in Thread '" + getCurrentThreadName() + "' failed in acquiring the lock.");
                return false;
            }
        } catch (InterruptedException e) {
            return false;
        } finally {
            if (acquired) {
                lock.unlock();
            }
        }
    }

    private void methodB() {
        System.out.println("Thread '" + getCurrentThreadName() + "' in methodB(...)");
    }

    private long getRandomWaitMillis() {
        return (long) (Math.random() * MAX_WAIT_MILLIS);
    }
}
