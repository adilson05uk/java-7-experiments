package com.thinkincode.java.concurrency;

import static com.thinkincode.java.utils.ThreadUtils.getCurrentThreadName;

public class DeadlockExperiment1 {

    public synchronized void methodA(DeadlockExperiment1 deadlockExperiment) {
        System.out.println("Thread '" + getCurrentThreadName() + "' in methodA(...)");
        deadlockExperiment.methodB();
    }

    public synchronized void methodB() {
        System.out.println("Thread '" + getCurrentThreadName() + "' in methodB(...)");
    }
}
