package com.thinkincode.java.concurrency;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.thinkincode.java.utils.ThreadUtils.getCurrentThreadName;

public class DeadlockExperiment3 {

    private final static long WAIT_MILLIS = 10L;

    private final Lock lock = new ReentrantLock();

    public void methodA(DeadlockExperiment3 deadlockExperiment) {
        boolean acquired = false;

        while (!acquired) {
            try {
                acquired = lock.tryLock(WAIT_MILLIS, TimeUnit.MILLISECONDS);

                if (acquired) {
                    System.out.println("Thread '" + getCurrentThreadName() + "' in methodA(...)");
                    deadlockExperiment.methodB();
                }
            } catch (InterruptedException e) {
                // nothing to do
            } finally {
                if (acquired) {
                    lock.unlock();
                }
            }
        }
    }

    public void methodB() {
        boolean acquired = false;

        while (!acquired) {
            try {
                acquired = lock.tryLock(WAIT_MILLIS, TimeUnit.MILLISECONDS);

                if (acquired) {
                    System.out.println("Thread '" + getCurrentThreadName() + "' in methodB(...)");
                }
            } catch (InterruptedException e) {
                // nothing to do
            } finally {
                if (acquired) {
                    lock.unlock();
                }
            }
        }
    }
}
