package com.thinkincode.java.concurrency;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Lock;

public class MicroBlogTimeline {

    @NotNull
    private final List<String> updates;

    @NotNull
    private final Lock lock;

    @NotNull
    private final String name;

    @Nullable
    private Iterator<String> iterator;

    MicroBlogTimeline(@NotNull String name,
                      @NotNull Lock lock,
                      @NotNull List<String> updates) {
        this.updates = updates;
        this.lock = lock;
        this.name = name;
    }

    public void addUpdate(@NotNull String update) {
        updates.add(update);
    }

    public void prepareIterator() {
        iterator = updates.iterator();
    }

    public void printTimeline() {
        lock.lock();

        try {
            if (iterator != null) {
                System.out.print(name + ": ");

                while (iterator.hasNext()) {
                    System.out.print(iterator.next() + ", ");
                }

                System.out.println();
            }
        } finally {
            lock.unlock();
        }
    }
}
