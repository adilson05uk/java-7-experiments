package com.thinkincode.java.concurrency;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.thinkincode.java.utils.ThreadUtils.getCurrentThreadName;

public class DeadlockExperiment2 {

    private final Lock lock = new ReentrantLock();

    public void methodA(DeadlockExperiment2 deadlockExperiment) {
        lock.lock();

        try {
            System.out.println("Thread '" + getCurrentThreadName() + "' in methodA(...)");
            deadlockExperiment.methodB();
        } finally {
            lock.unlock();
        }
    }

    public void methodB() {
        lock.lock();

        try {
            System.out.println("Thread '" + getCurrentThreadName() + "' in methodB(...)");
        } finally {
            lock.unlock();
        }
    }
}
