package com.thinkincode.java.groovy;

import com.thinkincode.Experiments;

public class GroovyExperiment {

    public void executeGroovyClass(String[] args) {
        Experiments.main(args);
    }
}
