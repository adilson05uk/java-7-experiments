package com.thinkincode.java.utils;

public class ThreadUtils {

    public static String getCurrentThreadName() {
        return Thread.currentThread().getName();
    }
}
