package com.thinkincode.java.performance;

public class CacheExperiment {

    private final int ARRAY_SIZE = 1024 * 1024;
    private final int[] array = new int[ARRAY_SIZE];

    public void run() {
        // Warm up the code
        for (int i = 0; i < 5; i++) {
            touchEachCacheLine();
            touchEachItem();
        }

        // Run the experiment
        for (int i = 0; i < 5; i++) {
            long t0 = System.nanoTime();

            touchEachCacheLine();

            long t1 = System.nanoTime();

            touchEachItem();

            long t2 = System.nanoTime();

            touchEachCacheLine();
            touchEachCacheLine();

            long t3 = System.nanoTime();

            touchEachItem();
            touchEachItem();

            long t4 = System.nanoTime();

            System.out.println("Time taken to touch each cache line once = " + (t1 - t0));
            System.out.println("Time taken to touch each item once = " + (t2 - t1));
            System.out.println("Time taken to touch each cache line twice = " + (t3 - t2));
            System.out.println("Time taken to touch each item twice = " + (t4 - t3));
            System.out.println();
        }
    }

    private void touchEachItem() {
        for (int i = 0; i < array.length; i++) {
            array[i]++;
        }
    }

    private void touchEachCacheLine() {
        for (int i = 0; i < array.length; i += 16) {
            array[i]++;
        }
    }
}
