package com.thinkincode.java.performance;

public class NanoTimeExperiment {

    public void printNanos() {
        long startMillis = System.currentTimeMillis();
        long startNanos = System.nanoTime();

        long previousMillis = startMillis;
        long previousNanos = startNanos;

        while (previousMillis < startMillis + 5) {
            long nowMillis = System.currentTimeMillis();
            long nowNanos = System.nanoTime();

            System.out.println("Now nanos = " + nowNanos);

            if (nowMillis != previousMillis) {
                System.out.println("Diff millis = " + (nowMillis - previousMillis));
                System.out.println("Diff nanos = " + (nowNanos - previousNanos));
                System.out.println();

                previousMillis = nowMillis;
                previousNanos = nowNanos;
            }
        }

        System.out.println("Total millis elapsed = " + (previousMillis - startMillis));
        System.out.println("Total nanos elapsed = " + (previousNanos - startNanos));
    }
}
