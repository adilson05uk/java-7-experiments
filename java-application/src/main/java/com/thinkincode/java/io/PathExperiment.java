package com.thinkincode.java.io;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class PathExperiment {

    public Path getZipUtilityPath() {
        return Paths.get("/", "usr", "bin", "zip");
    }

    public Path getCurrentWorkingDirectoryPath() {
        return Paths.get(".").toAbsolutePath().normalize();
    }

    public List<String> getNamesOfFilesMatchingGlob(Path directoryPath, String filenameGlob) throws IOException {
        List<String> filenames = new ArrayList<>();

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(directoryPath, filenameGlob)) {
            for (Path entry : stream) {
                filenames.add(entry.getFileName().toString());
            }
        }

        return filenames;
    }

    public void printInformationAboutPath(Path path) {
        System.out.println("----- Information about path -----");
        System.out.println("File Name [" + path.getFileName() + "]");
        System.out.println("Number of Name Elements in the Path[" + path.getNameCount() + "]");
        System.out.println("Parent Path [" + path.getParent() + "]");
        System.out.println("Root of Path [" + path.getRoot() + "]");
        System.out.println("Subpath from Root, 2 elements deep[" + path.subpath(0, 2) + "]");
        System.out.println("-----");
    }
}
