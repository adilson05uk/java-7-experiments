package com.thinkincode.java.io;

import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

public class WatchServiceExperiment {

    public void watchFile(Path path) {
        try {
            WatchService watchService = FileSystems.getDefault().newWatchService();
            path.register(watchService, ENTRY_MODIFY);

            while (true) {
                WatchKey watchKey = watchService.take();

                for (WatchEvent<?> watchEvent : watchKey.pollEvents()) {
                    System.out.println("WatchEvent:");
                    System.out.println("Kind = " + watchEvent.kind());
                    System.out.println("Context = " + watchEvent.context());
                }

                watchKey.reset();
            }
        } catch (IOException | InterruptedException e) {
            System.out.println(e);
        }
    }
}
