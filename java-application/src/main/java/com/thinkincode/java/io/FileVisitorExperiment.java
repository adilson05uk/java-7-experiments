package com.thinkincode.java.io;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class FileVisitorExperiment {

    public List<String> getNamesOfFilesMatchingGlob(Path directoryPath, String fileNameGlob) throws IOException {
        List<String> matchedFilenames = new ArrayList<>();
        PathMatcher filenameMatcher = FileSystems.getDefault().getPathMatcher("glob:" + fileNameGlob);

        Files.walkFileTree(directoryPath, new FindFilesFileVisitor(matchedFilenames, filenameMatcher));

        return matchedFilenames;
    }

    private static class FindFilesFileVisitor extends SimpleFileVisitor<Path> {

        private final List<String> matchedFilenames;
        private final PathMatcher filenameMatcher;

        FindFilesFileVisitor(List<String> matchedFilenames,
                             PathMatcher filenameMatcher) {
            this.matchedFilenames = matchedFilenames;
            this.filenameMatcher = filenameMatcher;
        }

        @Override
        public FileVisitResult visitFile(Path filePath, BasicFileAttributes attributes) {
            Path filenamePath = filePath.getFileName();

            if (filenameMatcher.matches(filenamePath)) {
                String filename = filenamePath.toString();
                matchedFilenames.add(filename);
            }

            return FileVisitResult.CONTINUE;
        }
    }
}
