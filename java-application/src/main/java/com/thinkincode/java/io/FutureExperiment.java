package com.thinkincode.java.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Path;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class FutureExperiment {

    public void readFileWithFuture(Path filePath) {
        try {
            AsynchronousFileChannel channel = AsynchronousFileChannel.open(filePath);
            ByteBuffer buffer = ByteBuffer.allocate(1_048_576);
            Future<Integer> result = channel.read(buffer, 0);

            while (!result.isDone()) {
                doSomeOtherStuff();
            }

            Integer bytesRead = result.get();
            System.out.println("Bytes read [" + bytesRead + "]");
        } catch (IOException | ExecutionException | InterruptedException e) {
            System.out.println(e.toString());
        }
    }

    private void doSomeOtherStuff() throws InterruptedException {
        System.out.println("Doing some other stuff.");
        Thread.sleep(1);
    }
}
