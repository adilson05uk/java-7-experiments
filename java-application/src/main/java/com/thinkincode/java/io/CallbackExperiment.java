package com.thinkincode.java.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.file.Path;

public class CallbackExperiment {

    public void readFileWithCallback(Path filePath) {
        try {
            AsynchronousFileChannel channel = AsynchronousFileChannel.open(filePath);
            ByteBuffer buffer = ByteBuffer.allocate(1_048_576);
            channel.read(buffer, 0, buffer, new MyCompletionHandler());

            doSomeOtherStuff();
        } catch (IOException | InterruptedException e) {
            System.out.println(e.toString());
        }
    }

    private void doSomeOtherStuff() throws InterruptedException {
        System.out.println("Doing some other stuff.");
        Thread.sleep(3000);
    }

    private static class MyCompletionHandler implements CompletionHandler<Integer, ByteBuffer> {

        @Override
        public void completed(Integer result, ByteBuffer attachment) {
            System.out.println("Bytes read [" + result + "]");
        }

        @Override
        public void failed(Throwable throwable, ByteBuffer attachment) {
            System.out.println("Failed reading bytes: " + throwable.toString());
        }
    }
}
