package com.thinkincode.java.io;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

public class FilesExperiment {

    public void createFileWithReadWritePermissions(Path directoryPath, String filename) throws IOException {
        Set<PosixFilePermission> permissions = PosixFilePermissions.fromString("rwxr-xr-x");
//        Set<PosixFilePermission> permissions = EnumSet.of(OWNER_READ, OWNER_WRITE, OWNER_EXECUTE, GROUP_READ, GROUP_WRITE);
        FileAttribute<Set<PosixFilePermission>> attribute = PosixFilePermissions.asFileAttribute(permissions);

        Path filePath = directoryPath.resolve(filename);

        Files.createDirectories(directoryPath, attribute);
        Files.createFile(filePath, attribute);
    }

    public void recursivelyDeleteDirectory(Path directoryPath) throws IOException {
        if (Files.notExists(directoryPath)) {
            return;
        }

        Files.walkFileTree(directoryPath, new DeleteFilesFileVisitor());
    }

    public void printInformationAboutFile(Path path) {
        System.out.println("----- Information about file -----");

        try {
            System.out.println(Files.getLastModifiedTime(path));
            System.out.println(Files.size(path));
            System.out.println(Files.isSymbolicLink(path));
            System.out.println(Files.isDirectory(path));
            System.out.println(Files.readAttributes(path, "*"));
        } catch (IOException ex) {
            System.out.println("Exception [" + ex.getMessage() + "]");
        }

        System.out.println("-----");

    }

    private static class DeleteFilesFileVisitor extends SimpleFileVisitor<Path> {

        @Override
        public FileVisitResult visitFile(Path filePath, BasicFileAttributes attributes) throws IOException {
            Files.delete(filePath);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path directoryPath, IOException exception) throws IOException {
            Files.delete(directoryPath);
            return FileVisitResult.CONTINUE;
        }
    }
}
